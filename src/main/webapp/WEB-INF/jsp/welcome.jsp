<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@page import="java.util.ArrayList"%>
<%--Importing all the dependent classes--%>
<%@page import="java.util.Iterator"%>

<!DOCTYPE html>
<html>
<head>
<title>AWS RDS Demo</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
img {
	max-width: 100%;
}

.selectWidth {
	width: 10ch;
	margin: 10px;
}

option {
	width: auto;
}
</style>
</head>
<body>

	<div>
		<h2 align="center">AWS RDS (mysql) data display</h2>
	</div>


	<div align="center">

		<table class="table table-dark table-striped" id="11">
			<thead>

				<tr>
					<th>
						<div align="center">
							<p style="color: red">Departments Table records:</p>
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				<%
					ArrayList<String> listDepts = (ArrayList) request.getAttribute("listDepts");
					// Iterating through subjectList

					if (listDepts != null) // Null check for the object
					{

						for (String recordData : listDepts) {
				%>
				<tr>
					<td><%=recordData%></td>
					<%
						}

						}
					%>
				
		</table>
		</tbody>
		</table>








	</div>

</body>
</html>
